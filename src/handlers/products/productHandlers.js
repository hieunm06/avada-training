const {
  getProductById,
  getProducts,
  deleteProductById,
  updateProduct,
  addProduct,
} = require('../../service/productRepository');

/**
 *
 * @param ctx
 */
async function getAll(ctx) {
  {
    /**
     * The API should return all the list of products in file products.json.
  The API should have the parameter limit with the request URL like /api/product?limit=5 then return only 5 first products
  The API should have the parameter limit with the request URL like /api/product?sort=desc or /api/product?sort=asc then return products order by createdAt field desc or asc
     */

    const { limit } = ctx.request.query;
    const orderBy = String(ctx.request.query.sort);

    const products = getProducts(Number(limit), orderBy);
    await ctx.render('products', {
      products,
    });
  }
}

/**
 *
 * @param ctx
 */
async function getOne(ctx) {
  try {
    const { id } = ctx.params;
    const getCurrentProduct = getProductById(Number(id));
    if (getCurrentProduct) {
      return (ctx.body = {
        data: getCurrentProduct,
      });
    }

    ctx.status = 404;
    return (ctx.body = {
      status: 'error!',
      message: 'Product Not Found with that id!',
    });
  } catch (e) {
    return (ctx.body = {
      success: false,
      error: e.message,
    });
  }
}

/**
 *
 * @param ctx
 */
async function deleteOne(ctx) {
  try {
    let { id } = ctx.params;
    id = Number(id);
    const deletedProduct = getProductById(id);
    if (deletedProduct) {
      deleteProductById(id);
      return (ctx.body = {
        message: 'Product Deleted',
      });
    }

    ctx.status = 404;
    return (ctx.body = {
      status: 'error!',
      message: 'Product Not Found with that id!',
    });
  } catch (e) {
    return (ctx.body = {
      success: false,
      error: e.message,
    });
  }
}

/**
 *
 * @param ctx
 */
async function updateOne(ctx) {
  try {
    let { id } = ctx.params;
    id = Number(id);
    if (updateProduct(id, JSON.parse(ctx.request.body))) {
      return (ctx.body = {
        message: 'Product Updated',
      });
    }

    ctx.status = 404;
    return (ctx.body = {
      status: 'error!',
      message: 'Product Not Found with that id!',
    });
  } catch (e) {
    return (ctx.body = {
      success: false,
      error: e.message,
    });
  }
}

/**
 *
 * @param ctx
 */
async function addOne(ctx) {
  try {
    addProduct(JSON.parse(ctx.request.body));
    return (ctx.body = {
      message: 'Product Added',
    });
  } catch (e) {
    return (ctx.body = {
      success: false,
      error: e.message,
    });
  }
}

module.exports = {
  getAll,
  getOne,
  deleteOne,
  updateOne,
  addOne,
};

const fs = require('fs');
const path = require('path');
const DB_PATH = path.join(__dirname, '../data/products.json');
let { data: products } = require(DB_PATH);

products = products.map((product) => {
  return { ...product, createdAt: new Date(product.createdAt).getTime() };
});

const saveToDb = (products) =>
  fs.writeFileSync(DB_PATH, JSON.stringify({ data: products }));

function getProducts(limit, sort) {
  if (sort && sort.toLocaleLowerCase() === 'desc') {
    products.sort((a, b) => b.createdAt - a.createdAt);
  } else {
    products.sort((a, b) => a.createdAt - b.createdAt);
  }
  if (Number(limit) > 0) {
    products.length = limit;
  }
  return products;
}

function getProductById(id) {
  return products.find((product) => product.id === id);
}

function deleteProductById(id) {
  const productIndex = products.findIndex((product) => product.id === id);
  products.splice(productIndex, 1);
  saveToDb(products);
}

function updateProduct(id, data) {
  let newProduct = false;
  for (product of products) {
    if (product.id === id) {
      newProduct = product;
      Object.keys(product).forEach((key) => {
        product[key] = data[key] ?? product[key];
      });
      break;
    }
  }
  if (newProduct) {
    saveToDb(products);
    return true;
  }
  return false;
}

function addProduct(data) {
  const newProdId = products.length + 1;
  products.push({
    ...data,
    id: newProdId,
  });
  saveToDb(products);
}

module.exports = {
  getProductById,
  getProducts,
  deleteProductById,
  updateProduct,
  addProduct,
};

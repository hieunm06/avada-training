const yup = require('yup');
// const {date,object} = yup;

// const today = new Date();

// const schema = object({
//   birthday: date().transform(parseDateString).max(today),
// });

// const isValid = schema.validateSync({
//   birthday: "2020-02-02",
// });

async function productInputMiddleware(ctx, next) {
  try {
    const postData = ctx.request.body;
    let schema = yup.object().shape({
      price: yup.number().positive().integer().required(),
      name: yup.string().required(),
      description: yup.string().required(),
      color: yup.string().required(),
      createdAt: yup.date().required(),
      image: yup.string().required(),
    });

    await schema.validate(postData);
    next();
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      success: false,
      errors: e.errors,
      errorName: e.name,
    };
  }
}

module.exports = productInputMiddleware;

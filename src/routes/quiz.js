const Router = require('koa-router');
const router = new Router();

router.get('/quiz', async (ctx) => {
  const users = require('../data/users');
  const posts = require('../data/posts');
  const comments = require('../data/comments');

  await ctx.render('quiz', {
    users,
    comments,
    posts,
  });
});

module.exports = router;

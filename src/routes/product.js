const Router = require('koa-router');

// Prefix all routes with /books
const router = new Router({
  prefix: '/api',
});

const productInputMiddleware = require('../middleware/productInputMiddleware');

const {
  getAll,
  getOne,
  deleteOne,
  updateOne,
  addOne,
} = require('../handlers/products/productHandlers');

// Routes will go here

//GET	/api/products	Get all list of products	limit, orderBy
router.get('/products', getAll);
//GET	/api/product/:id	Get one product by ID	fields
router.get('/product/:id', getOne);
// DELETE	/api/product/:id	Delete a product of a given id	None
router.delete('/product/:id', deleteOne);
/// PUT	/api/product/:id	Update a product with the input data	None
router.put('/product/:id', productInputMiddleware, updateOne);
/// // POST	/api/products	Create a new product to the list	None
router.post('/products', productInputMiddleware, addOne);

module.exports = router;

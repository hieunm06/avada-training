// run `node index.js` in the terminal

const Koa = require('koa');
const koaBody = require('koa-body');
const app = new Koa();
app.use(koaBody());

const render = require('koa-ejs');
const path = require('path');
render(app, {
  root: path.join(__dirname, 'view'),
  layout: false,
  viewExt: 'html',
  cache: false,
  debug: false,
});

const productsRoute = require('./src/routes/product');
const quizRoute = require('./src/routes/quiz');
app.use(productsRoute.routes());
app.use(quizRoute.routes());
app.use(async (ctx) => {
  await ctx.render('index');
});

app.listen(5000);
